'''
Created on Aug 25, 2011

@author: david
'''

from pspy.gae.actiondomain import PspyAction

class Index(PspyAction):
    """
    Default Action 
    spawns index.gen
    """
    def execute(self):
        req = self.request
    
        req.setAttribute("results","100::Ok")
        req.setResponsePage("/${folder}/Index.gen")        
        
        return req.getResponse()
    
    def sess_check(self):
        return True