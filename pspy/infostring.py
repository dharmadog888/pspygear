'''
Created on Nov 22, 2009

@author: david
'''

class InfoString(object):
    """
    Standard alternative to query string formats to allow 
    posting queries in queries
    """
    # delimitters for fields 
    # and name value pairs
    fld = '::'
    nvp = '~'
    
    @classmethod
    def parse(cls, info):
        """
        Parse string into dict
        """
        si = {}
        for p1 in info.split('::'):
            p2 = p1.split('~')
            key = p2[0]
            
            # normalize addr element for siteinfo
            if key.lower().startswith('addr'):
                key = 'address'
                
            si[key] = p2[1]

        return si
    
    @classmethod
    def format(cls, si):
        """
        Format string from dict 
        """
        info = ''
        for k, v in si.items():
            if len(info) > 0:
                info += cls.fld
            info = "%s%s%s%s" % (info, k, cls.nvp, v)
            
        return info        