__version__ = "0.0.1"

from notify import Notification
from infostring import InfoString

__all__ = ['Notification', 'InfoString']

CONTENT_TYPES = { "json":"application/json",
                  "html":"text/html",
                  "xml":"text/xml",
                  "xhtml":"text/xml",
                  "jpg":"image/jpeg",
                  "jpeg":"image/jpeg",
                  "png":"image/png",
                  "gif":"image/gif",
                  "pdf":"application/pdf",
                  "bin":"application/octetstream",
                  "text":"text/plain",
                  "plain":"text/plain",
                  "css":"text/css",
                  "less":"text/css",
                  "js":"text/javascript",
                  "javascript":"text/javascript",
                  "code":"text/plain"
                }