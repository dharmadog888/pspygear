'''
Created on Aug 24, 2011

@author: david
'''
import webapp2
 
from google.appengine.ext.webapp.util import run_wsgi_app
import logging

PSPYGEAR_YAML = "pspygear.yaml"
LINKAGE_ROOT_NODE = "ROOT"
STORE_KEY = "webapp2_extras.sessions"
SECRET_KEY = "203948adofksdfs"
CFG = {STORE_KEY:{"secret_key":SECRET_KEY,}}

class Server(object):
    
    def __init__(self,applets,debug=False):
        self._dispatchApplets = applets
        self._applets = {}
        self._debug = debug
        
        logging.info("-- Server initialized")
            
    def getApp(self):
        """
        create and return a WSGI compliant app.
        
        -dkc-
        The GAE for Python 2.7 wants the raw web app.
        """
        return webapp2.WSGIApplication(self._dispatchApplets,config=CFG)
    
    def run(self):
        
        # create web app from handler       
        wa = webapp2.WSGIApplication(self._dispatchApplets,config=CFG, debug=self._debug)
    
        logging.info("-- starting server")
 
        # and run it
        run_wsgi_app(wa)
    
    