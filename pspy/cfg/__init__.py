"Find and load runtime configuration"
import os, os.path

# init 
pspyConf = None

class FakeConf(object):
	mailInfo = popInfo = "No Mail"
	dbDriver = dbRef = None

CONF_FILE = 'pspygear.cfg'
CONF_DIRS = ['.', './cfg', './config', './conf', './etc']
CONF_ENV_VAR = "PSPYGEAR"
CONF_PATH = None

gear = os.getenv(CONF_ENV_VAR)

# if we find global one, use it
if gear and os.path.isfile(gear):
	CONF_PATH = gear

# look in each of the default locations
for d in CONF_DIRS:
	tstPath = "%s%s%s" % (d, os.sep, CONF_FILE)
	if os.path.isfile(tstPath):
		# override with local
		CONF_PATH = tstPath
		break

# load file	
try:
	if CONF_PATH:
		execfile(CONF_PATH)
		pspyConf = PspyGearConf()
	else:
		raise
except:
	pspyConf = FakeConf()
	
__all__ = ['pspyConf']
