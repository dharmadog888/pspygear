//------------------------------------------------------------
// pspy.js
//
// PspyGear Common JS Utilities  
//
//------------------------------------------------------------
var DEBUG_FLAG = true;

//------------------------------------------------------------
// String formatting essentials
//  
// Hint:  Use format for moneyish things like $45.00
//------------------------------------------------------------
// String.format();
// format using positionals in {}.
//
// example: "Hello {0}! Want to {1}?".format("World","Party");
//      ==> "Hello World! Want to Party?"
String.prototype.format = function() {
    var formatted = this;
    for (var i = 0; i < arguments.length; i++) {
        var regexp = new RegExp('\\{'+(i+1)+'\\}', 'gi');
        formatted = formatted.replace(regexp, arguments[i]);
    }
    return formatted;
};

//------------------------------------------------------------
// String.$$();
// format using positionals using $.
//
// example: "Hello $0! Want to $1?".$$("World","Party");
//   ==> "Hello World! Want to Party?"
String.prototype.$$ = function() {
    var formatted = this;
    for (var i = 0; i < arguments.length; i++) {

        var regexp = new RegExp('\\$'+(i+1), 'gi');
        formatted = formatted.replace(regexp, arguments[i]);
    }
    return formatted;
};

String.prototype.trim = function(chr) {
    return this.ltrim(chr).rtrim(chr);
        
};

// RegEx Testing Patterns
var _PWDCHK_UC = /[A-Z]/;
var _PWDCHK_NM = /[0-9]/;
var _PWDCHK_LC = /[a-z]/;
var _PWDCHK_SP = /[~!@#$%^&*_]/;
var _PWDCHK_LEN = 6;

String.prototype.pwdFormatCheck = function() {
    return this.length < _PWDCHK_LEN ? "Password is too short (Must be $1 characters or longer)".$$(_PWDCHK_LEN) :
           !this.match(_PWDCHK_UC) ? "Password must contain at least one upper case character" :
           !this.match(_PWDCHK_LC) ? "Password must contain at least one lower case character" :
           !this.match(_PWDCHK_NM) ? "Password must contain at least one numeric character" :
           !this.match(_PWDCHK_SP) ? "Password must contain at least one special character (~!@#$%^&*_)" :
           null;            
};

String.prototype.ltrim = function(chr) {
    if (!chr || this.replace(/^\s+/,"") === "") {
        return this.replace(/^\s+/,"");
    }
    
    rv = this;
    
    while (rv.indexOf(chr) === 0) {
        rv = rv.substring(chr.length);
    }
    
    return rv;
};
    
String.prototype.rtrim = function(chr) {
    if (!chr || this.replace(/\s+$/,"") === "") {
        return this.replace(/\s+$/,"");
    }
    
    rv = this;
    l = rv.length;
    while (rv.lastIndexOf(chr) === l - chr.length) {
        rv = rv.substring(0,l-chr.length);
        l = rv.length;
    }
    
    return rv;

};

//----------------------------------------
// Enhanced selecting
//----------------------------------------
HTMLSelectElement.prototype.sort = function() {
    // get all options into an array
    ar = {};
    ndx = [];
    for (i=0; i<this.length; i++) {
        o = this.options[i];
        ar[o.text] = o;
        ndx[i] = o.text;
    }
    ndx.sort();
    this.length=0;
    for(v=0;v<ndx.length;v++)
        this.options.add(ar[ndx[v]]);
}

HTMLSelectElement.prototype.sortable = false;
HTMLSelectElement.prototype.addSort = function(el){ this.add(el);this.sort() }
HTMLSelectElement.prototype.removeSort = function(el){ this.remove(el);this.sort() }

HTMLSelectElement.prototype.value = function() { return this.options[this.selectedIndex].value; }
//----------------------------------------
// Logging utility
//----------------------------------------
function $$log(txt) {
    if(DEBUG_FLAG)
        if(console)
            console.log(txt);
}

//---------------------------------------------------------------
// global go function to fetch chunks of HTML (deprecated)
//---------------------------------------------------------------
 function go(anch) {
     return pgPage.go(anch);
 }
