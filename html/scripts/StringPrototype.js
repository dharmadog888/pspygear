//------------------------------------------------------------
// String formatting essentials
//  
// Hint:  Use format for moneyish things like $45.00
//------------------------------------------------------------
// String.format();
// format using positionals in {}.
//
// example: "Hello {0}! Want to {1}?".format("World","Party");
//      ==> "Hello World! Want to Party?"
String.prototype.format = function() {
    var formatted = this;
    for (var i = 0; i < arguments.length; i++) {
        var regexp = new RegExp('\\{'+(i+1)+'\\}', 'gi');
        formatted = formatted.replace(regexp, arguments[i]);
    }
    return formatted;
};

//------------------------------------------------------------
// String.$$();
// format using positionals using $.
//
// example: "Hello $0! Want to $1?".$$("World","Party");
//   ==> "Hello World! Want to Party?"
String.prototype.$$ = function() {
    var formatted = this;
    for (var i = 0; i < arguments.length; i++) {

    	var regexp = new RegExp('\\$'+(i+1), 'gi');
        formatted = formatted.replace(regexp, arguments[i]);
    }
    return formatted;
};

String.prototype.trim = function(chr) {
    return this.ltrim(chr).rtrim(chr);
        
};

String.prototype.ltrim = function(chr) {
    if (!chr || this.replace(/^\s+/,"") === "") {
        return this.replace(/^\s+/,"");
    }
    
    rv = this;
    
    while (rv.indexOf(chr) === 0) {
        rv = rv.substring(chr.length);
    }
    
    return rv;
};
    
String.prototype.rtrim = function(chr) {
    if (!chr || this.replace(/\s+$/,"") === "") {
        return this.replace(/\s+$/,"");
    }
    
    rv = this;
    l = rv.length;
    while (rv.lastIndexOf(chr) === l - chr.length) {
        rv = rv.substring(0,l-chr.length);
        l = rv.length;
    }
    
    return rv;

};
