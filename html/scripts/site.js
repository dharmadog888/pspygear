//------------------------------------------------------------
//site.js
//
// Site Navigation Module
//      and other
//   Global Resources
//       for the
//    End PspyGears Project site 
//
//------------------------------------------------------------
//------------------------------------------------------------
// Global Variables
var DEBUG_FLAG = true;

var pgPage = null;
var pgNav = null;

//------------------------------------------------------------
// Miscellaneous things needed on multiple pages
var newsStart = 0;
var newsCount = 3;

//--------------------------------------------------------------
// Application Classes
//------------------------------------------------------------

/**
 * Base class for all site page modules.
 * 
 * Extended and constructed in each of the 
 * 'Context' scripts to provide the services 
 * represented by the context and usually 
 * managed via the Context Menu ('connav').
 */
var SitePage = new Class ({
	name: "None",
	
	/** constructor */
	initialize: function(pageName) {
		// set up internals
		this.name = pageName;
	},
	
	/**
	 *  chunk fetcher  
	 *
	 * expects to receive an element with action and
	 * target attributes that will fetch HTML and 
	 * replace content of target element
	 *
	 */	
	 go: function (anch) {
		
		$$log("~~ Fetching $1 Action=$2 Target=$3");
		
		new Request.HTML({
			method: "post",
			   url: anch.getAttribute('url'),
			  data: "action=$1&chunk=1".$$(anch.getAttribute('action')),			
			update: anch.getAttribute('target'),
			
			onComplete: function(order,txt) {	
				$$log("~~ Chunk Loaded");
			}
			
		}).send();
		
		// disable browser response
		return false;
	},

	/**
	 * Load navigational UI
	 */
	loadNav: function(url, module, target) {
		this.go( new Element('anch',{'url':url, 'action':'Load&page=$1.gen'.$$(module), 'target': target ? target : 'menubar' }));
	},
	
	//custom convenience functions
	showForgotBox: function() {
		el = $('forgotdiv').clone();
		
		ychPage.forgot = new FormCheck(el);
		el.setStyle('display','block');
		el.MooDialog({
			focus: true,
			'class': 'forgotDialog',
			
			onClose: function() {
				ychPage.clearContents([$$('.fc-tbx'),$$('.fc-error')]);
			}
		});
	},
	
	askForEmail: function(form){
		// check form validation.
    	if (!ychPage.forgot || !ychPage.forgot.isFormValid()) {
    		return false;
    	}
		
		jsform = form;
		jsform.action = '/YCH?action=access.Reset&step=1&ea=$1'.$$(form.elements['ea'].value);
		
		req = new Form.Request(jsform, $(form).getParent()).send();
		
		return false;
	},
	
	postProcForm: function(){
		// setup selectors
		$$('select').each(function(sel,sndx) {
			// check each select option
			for(ondx=0;ondx<sel.options.length;ondx++) {
				opt = sel.options[ondx];
				try {
					opt.selected = (opt.attributes['selectme'].value  == 'True');
					if(opt.selected && !sel.multiple)
						sel.selectedIndex = ondx;
				}
				catch(err) {
					opt.selected = false;
				}
			}
		});
		
		// setup checkboxes
		$$("input[type=checkbox]").each(function(cbx){
			cbx.checked = (cbx.attributes['checkme'] && cbx.attributes['checkme'].value == 'True');
		});			
		
		// and radio buttons
		$$("input[type=radio]").each(function(rbtn) {
			rbtn.selected = (rbtn.attributes['selectme'] && rbtn.attributes['selectme'].value == "True");
		});
	},
	
		
	/**
	 * Display Alert Message
	 */
	alert: function(msg) {
		this.flashMessage(msg).set('style','color: #f00');
	},
	
	/**
	 * Ajax updates to server...
	 */
	flashMessage: function(msg) {
		alert(msg);
	},
	
	errorMessage: function(msg) {
		alert(msg);
	},
	
	/**
	 * pop a window, fill w/div,
	 * print and close...
	 * 
	 */
	printDiv: function(divName){
		wndw = window.open();
		wndw.document.write($(divName).innerHTML);
		wndw.print();
		wndw.close();
	},
	

	loadcontent: function(el,container,selected){
	    if (selected){
	        $$('.selected').removeClass('selected');
	        $(el).addClass('selected');
	    }
	    this.clearContents([$$('.fc-tbx'),$$('.fc-error')]);
	    
	    $(container).empty();
	    $(container).load($(el).get('url'));
	},
	
	clearContents: function(arraymap) {
		arraymap.each(function(item1){
			item1.each(function(item2){
				item2.empty();
			});
		});
	},
	
	loadDialogPage: function(name){
        if ($$('.MooDialog').length > 0) {
            $$('.MooDialog')[0].load(page);
        } else {
            div = new Element('div').MooDialog();
            div.load(page); 
        }	        
	},

	loadDialog: function(html){
        if ($$('.MooDialog').length > 0) {
            $$('.MooDialog')[0].innerHTML = html;
        } else {
            div = new Element('div').MooDialog();
            div.innerHTML = html; 
        }	        
	}
	    
});


