//-------------------------------------
// G6 Blog Portal
// PspyGear Applet
// 
// init.js 
//   - Initialize main landing page
// 
// David Connell
//-------------------------------------

// Global Resources
var g6page = null;
var g6nav = null;

// Logging utility
function $$log(txt) {
	console.log(txt);
}

// lights, action, camera!
function load() {
	// set navigation module
	g6nav = new NAV();
	
	// set page module
	g6page = new G6Page();
	
	// display the page
	return g6page.go();
}
