//-------------------------------------
// G6 Blog Portal
// PspyGear Applet
// 
// index.js 
//   - landing page module
// 
// David Connell
//-------------------------------------

var G6Page = new SitePage({	
	
	/** constructor */
	initialize: function() {
		// set up internals
		this.parent('G6');
	},


	/*---------------------------------
	 * Manage page initialization
	 */
	go: function() {
			// nav block
			this.loadLeft();
	
			// pick a quote
			this.loadRight()
			
			// activate login form
			this.setupLogon();
			
			return this;
		},

	/*-----------------------
	 * Set up for login via
	 * AJAX / JSON.
	 */
	setupLogon: function() {		
		lgn = $$('#logon form');
		
		// add AJAX handler
		new Request.HTML({
			
			}).send();
		});
		
		return;
	},
	
	/*------------------------
	 * Load right sidbar with
	 * quote of the moment
	 */
	loadLeft: function(){
		// turn off nav 
		$('nav').set('class','hidden').empty();
		
		// turn off blog list
		$('bloglist').set("class","hidden").empty();
		
		// activate login
		$('logon').set('class',"visible");
	},
	
	/*------------------------
	 * Load right sidbar with
	 * quote of the moment
	 */
	loadRight: function(){
		rd = $('quote').set('text','Those who talk do not know.');		
	}
	
});


//-------------------------------------------
//Show time!
window.addEvent('domready', function(){
	// starting small
	pgPage = new G6Page();
});

