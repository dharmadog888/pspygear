//-------------------------------------
// G6 Blog Portal
// PspyGear Applet
// 
// blogger.js 
//   - manage the main blog page
// 
// David Connell
//-------------------------------------

var G6Page = new Class({

	/*---------------------------
	 * Page Properties
	 */
	navList: ["Home","Account","New Blog", "Logout"],
	currBlogNm: "Default",
	currBlog: null,
	articles: null,
	
	/*---------------------------------
	 * Manage page initialization
	 */
	go: function() {
			// nav block
			this.loadLeft();

			// main content
			this.loadMain();
			
			// pick a quote
			this.loadRight()
			
			// utility timer
			this.timer = null;
			
			return this;
		},
	/*-------------------------
	 * Manage navigation opts
	 */
	loadLeft: function(){
		// load up navigation
		g6nav.load(this.navList);
		
		// load blog list
		this.listBlogs();
	},
	
	/*---------------------
	 * Interval function to
	 * wait for element.
	 */
	setupNew: function(elemid) {
		// wait for it...
		frm = null;
		
		frm = $$(elemid);
		if (frm){
			// got it!
			clearInterval(this.timer);
			
			// set up the new blog form
			frm.addEvent("submit",function(ev) {
				ev.stop();
				
				this.set('send', {
		            onSuccess: function(txt,xml){
		            	g6nav.sendThenGo(txt,"Blog");
		            } 
				}).send();
			});
		}
	},

	loadNew: function() {
		// fetch new blog info
		$('main').load("/G6?action=blog.New");

		// when complete, set up the form for AJAX
		this.timer = setInterval(this.setupNew('#newblog form'),250);
		
		// done here...
		return;
	},
	
	/*---------------------------
	 * set new post form form 
	 */
	setupForm: function() {
		frm = $$('#bloginput form');
		
		// add AJAX handler
		frm.addEvent("submit",function(ev) {
			ev.stop();
			
			this.set('send', {
	            onSuccess: function(txt,xml){
					// parse out JSON results
					rslt = JSON.decode(txt);
					blog = JSON.decode(rslt.blog);
					
					$$log( blog );
					$$log( rslt.blog );
					$$log( blog.name);
					$$log( rslt.blog.name);
					
	            	g6nav.loadBlog(blog.name);
	            } 
			}).send();
		});
		
		return;
		
	},
	
	/*-----------------------
	 * Manage main content
	 */
	loadBlog: function(){
		// fetch current blog info
		req = new Request.JSON({
			method: "post",
			   url: "/G6",
			  data: "action=blog.Fetch",
			update: "articles",
			
			onSuccess: function(json,txt) {
				// parse out JSON results
				blog = json.blog);
				arts = json.articles;
				
				g6page.listArticles(blog,arts);
			}
		}).send();	
	},
	
	/*------------------------
	 * Load right sidbar with
	 * quote of the moment
	 */
	loadRight: function(){
		// quote
		q = $('quote').set('text','Form is Emptiness and Empiness, Form.  ~ Heart Sutra');
	},
	
	listArticles: function(blog,arts){
		try {
			$("text").set("value","Put it here...");
			$('subj').set('value','');
			$('tags').set('value','');
			$('blogtitle').set('text',blog.name);
			$('tagline').set('text',blog.description)
			$('articles').set('class','visible');
	
			// clear any current content
			articles = $('articles').empty();
			
			new Element("h2", {text:"Previous Posts:"}).inject(articles);

			
			// iterate through articles
			if (arts.length > 0) 
				for (i=0;i<arts.length;i++) 
					this.formatArticle(JSON.decode(arts[i]), articles);
			else 
				// empty blog
				articles.set("text","No articles");
		}
		catch(ex) { 
			// no articles / no blog
			$('articles').set("class", "hidden");
		}
	},
	
	/*----------------------------------
	 *  create and inject aarticle div
	 */
	formatArticle: function (article, articles) {
	
		// create new div	
		d = new Element("div",{"class": "article","id": "D-" + article.aid}).inject(articles);
		
		// set up post reader
		txt = (article.subj == "") ? article.text : article.subj + ":\n\n" + article.text;
		l1 = new Element("label",{"for":"R-"+article.aid,"text":article.aid,"class":"articleId"}).inject(d);
		a = new Element('textarea',{
				text: txt,
				readonly: '1',
				cols: 100,
				rows: 10,
				name: "R-"+article.aid,
				id: "R-"+article.aid							
			}).set("class","articleTxt").inject(d);
		
		// display tags
		t = new Element('input',{
				value: article.tags, 
				readonly: '1',
				length: '100',
				name: "T-"+article.aid,
				id: "T-"+article.aid							
			}).set("class","articleTag").inject(d);
		},
		
	/*------------------------
	 * Fetch and display the 
	 * users list of blogs.
	 */
	listBlogs: function(){
		bl = $('bloglist').set('class','bloglist');

		// fetch list
		req = new Request({
			url: "/G6?action=blog.List",
		
			onSuccess: function(txt,xml) {
				// parse out JSON results
				rslt = JSON.decode(txt);
				blogs = rslt.blogs; 
				
				try {
					// loop through blog list
					
					if (blogs.length > 0) 
						for (i=0;i<blogs.length;i++) {
							blog = JSON.decode(blogs[i]);
							
							// add to listing
							a = new Element('a',{
								href: "javascript:g6nav.loadBlog('" +blog.name + "');", 
								text: blog.name,
								title: blog.description
							}).inject(new Element("li",{"style":'text-align:left;text-decoration:none'}).inject($('myblogs')));
							
							if (blog.default)
								a.set('class','currblog');
						}
					else 
						// new account maybe?
						bl.set("text","Blog List<br/>No Blogs Found");
				}
				catch(ex) {
					// no blogs...
					$('bloginput').empty().set("text","You currently have no Blogs to view. Click on the New Blog link on the Navigation panel to create one.");
				}
			}
		}).send();
	}
});