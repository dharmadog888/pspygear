//-------------------------------------
// G6 Blog Portal
// PspyGear Applet
// 
// account.js 
//   - Manage the user profile
// 
// David Connell
//-------------------------------------

var G6Page = new Class({

	/*---------------------------
	 * Page Properties
	 */
	initialize: function () {
		this.navList = ["Back"];
	},
	
	/*---------------------------------
	 * Manage page initialization
	 */
	go: function() {
			// nav block
			this.loadLeft();
			
			// main content
			this.loadMain();
	
			// pick a quote
			this.loadRight()
			
			return true;
		},

	/*------------------------
	 * Configure the accounts
	 * page and provide the
	 * smarts.
	 */
	loadLeft: function(){
		// turn off blog list
		$('bloglist').set("class","hidden").set('text','');
		
		// deactivate login
		$('logon').set('class','hidden').set('text','');
		
		// load up navigation
		g6nav.load(this.navList);
				
	},

	/*-------------------------
	 * Build navigation opts
	 */
	loadMain: function(){
		var msg = '';
		var msgClass = "statusMsg";
		
		// find the form
		frm = $$('#acctForm form');
				
		// add AJAX handler
		frm.addEvent("submit",function(ev) {
			ev.stop();
			
			this.set('send', {
	
	            onSuccess: function(txt,xml) {            	
	            	// parse results text
	            	jso = JSON.decode(txt)
	            	rs = jso.results.split("::");
	
	            	// check result code
	            	if (rs[0] == '200') {
	            		msg = "Updates Saved";
	            	}
	            	else {
	            		// error from back end
	            		msg = rs[1];
	            		msgClass = 'errorMsg';
	            	}	            	
	            	
	            	// render messages (or hide empties)
	            	$('messages').set('text',msg).set('class',msg == '' ? 'hidden' : msgClass);
	            }
	        }).send();
		});
	},
	
	/*------------------------
	 * Load right sidbar with
	 * quote of the moment
	 */
	loadRight: function(){
		rd = $('quote').set('text','Those who know do not talk.');	
	}
	
});
