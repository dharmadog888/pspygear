//-------------------------------------
// G6 Blog Portal
// PspyGear Applet
// 
// index.js 
//   - landing page module
// 
// David Connell
//-------------------------------------

var GUEST_ACCT = { name: "Guest Visitor", fname: "Guest", lname: "Visitor" };
var DEFAULT_COUNT = 5;

var G6Page = new Class({	

	Extends: SitePage,
	
	/** constructor */
	initialize: function() {
		// set up internals
		this.parent('G6');
		this.currAcct = this.fetchAcct();
		this.currBlog = null;
		
		$('accountname').set('text',GUEST_ACCT.name);
	},

	/**
	 * Fetch current user (or guest)
	 * from server
	 */
	fetchAcct: function() {
		// TODO: implement function
		return GUEST_ACCT;
	},
	
	/**
	 * Set the current account and 
	 * associated logon links.
	 */
	setCurrAcct: function(acct) {
		this.currAcct = acct;
		
		// show account name
		$('accountname').set('text',acct.name);
		
		// adjust access links
		this.hideLogon(acct.name != GUEST_ACCT.name);
		
			
	},
	
	//-------------------------------------
	// Account login management functions
	//-------------------------------------
	
	/**
	 * Display Login Form
	 */
	showLogon: function() {
		pgPage.lgnForm = $('logonform').clone();
		this.dialog = new MooDialog({
			'title':"Member Login"
		}).setContent(pgPage.lgnForm);
		
		this.dialog.open();
	},
	
	/**
	 * Hide login form and display correct
	 * link to allow sign in or out.
	 */
	hideLogon: function(loggedIn) {
		if(loggedIn){
			$('logonreq').setStyle("display","none");
			$('logoffreq').setStyle("display","block");
			$('poster').setStyle('display','none');
			$('postcollapse').setStyle('display','none');
			$('postexpand').setStyle('display','block');
		}
		else{
			$('logonreq').setStyle("display","block");
			$('logoffreq').setStyle("display","none");		
			$('poster').setStyle('display','none');
			$('postcollapse').setStyle('display','none');
			$('postexpand').setStyle('display','none');
		}
	},
	
	/**
	 * Log out current account
	 */
	logout: function(){
		this.setCurrAcct(GUEST_ACCT);
	},

	/**
	 * Form expandsion 
	 */
	expandPostForm: function(){
		$('poster').setStyle('display','block');
		$('postcollapse').setStyle('display','block');
		$('postexpand').setStyle('display','none');
	},
	

	/**
	 * Hide posting form
	 */
	collapsePostForm: function(){
		$('poster').setStyle('display','none');
		$('postcollapse').setStyle('display','none');
		$('postexpand').setStyle('display','block');
	},
	
	/**
	 * Process login form and process
	 * accordingly
	 */
	logon: function(frm) {		
		qs = frm.toQueryString();
		$$log("~~ " + qs);
		
		// add AJAX handler
		new Request.JSON({
			method: "post",
			   url: "/G6",
			  data: "action=account.Logon&$1".$$(qs),			
			
			// good response
			onSuccess: function(json,txt) {
				// check for successful login
				if (json.results == '200'){
					pgPage.setCurrAcct(json.acct);
				}
				else {
					alert(json.message);
				}
			},
			
			// not so good response
			onError: function(text) {
				alert(text);
			}
		}).send();

	
		pgPage.dialog.close();
		
		return;
	},
	
	//-----------------------
	// Manage main content
	//-------------------------
	
	/**
	 * Load requested blog articles from 
	 * server and determine if current 
	 * account can post to it.
	 */
	loadArticles: function(blog_id,start,count){
		// check params
		if (!start)
			start = 0;
		if (!count)
			count = DEFAULT_COUNT;
		
		// fetch current blog info
		req = new Request.JSON({
			method: "post",
			   url: "/G6",
			  data: "action=blog.Fetch",
			update: "articles",
			
			onSuccess: function(json,txt) {
				// parse out JSON results
				blog = json.blog;
				arts = json.articles;
				
				pgPage.currBlog = blog.bid;
				pgPage.listArticles(blog,arts);
				pgPage.checkPostable()
			}
		}).send();	
	},
	
	/**
	 * Load list of active blogs for
	 * current account view. 
	 */
	loadBlogList: function(){
		// is Guest:
		if (this.currAcct.name == "Guest"){
			
		}
		else{
			
		}
	},
	
	/**
	 * Post new article for current blog
	 */
	post: function(){
		
	}
	
});


//-------------------------------------------
//Show time!
window.addEvent('domready', function(){
	// starting small
	pgPage = new G6Page();
});

