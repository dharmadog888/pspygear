//-------------------------------------
// G6 Blog Portal
// PspyGear Applet
// 
// nav.js 
//   - Site navigation module
// 
// David Connell
//-------------------------------------
var NAV = new Class({
	
	Binds: ['loadAccount', 'logOut', 'addBlog', 'hide', 'backup', 'cancel'],
	
	/*=================
	 * Constructor
	 */
	initialize: function() {
		this.navLinks = { "Home":"home","Account":"loadAccount","New Blog":"newBlog","Logout":"logout", "Back":"backup", "Cancel": "cancel" };
		this.page = "/G6";
	},
	
	loadBlog: function(name) {
		$$log("~~ Loading: " + name);
		
		// fetch current blog info
		req = new Request({
			url: "/G6?action=blog.Fetch&name=" + name,
			
			onSuccess: function(txt,xml) {
				// parse out JSON results
				rslt = JSON.decode(txt);
				blog = JSON.decode(rslt.blog);
				arts = rslt.articles;
				
				g6page.listArticles(blog,arts);
			}
		}).send();	
	},
	
	/*---------------------------------
	 * Go to account maintenance page
	 */
	loadAccount: function (){
		this.loadAction('account.Edit');
	},

	/*---------------------------
	 * Goto main blog page
	 */
	home: function() {
		this.loadAction('Blog');
	},
	
	/*-----------------------
	 * Graceful exit link
	 */
	logout: function() {
		this.loadAction('account.Logout');
	},

	/*-----------------------
	 * New Blog link
	 */
	newBlog: function() {
		g6page.loadNew();
		this.load(['Cancel']);
	},
	
	/*-----------------------
	 * cancel new blog
	 */
	cancel: function() {
		this.loadAction('Blog');
	},
	
	/*---------------------------
	 * Make element go bye-bye
	 */
	hide: function () {
		$('nav').set('class','hidden');
		return this;
	},
	
	/*---------------------------
	 * Hello Nav!
	 */
	show: function() {
		$('nav').set('class','nav');
		return this;
	},
	
	/*-----------------------------------
	 * Build navigation panel for page
	 */
	load: function (navList) {
		// reset navigation slate
		nav = $('nav').empty();
		
		// add heading
		Element("strong",{"text": "Navigation"}).inject(nav);
		
		// add in list container
		Element("ul",{"id":"navlist", "style":'text-align:left;text-decoration:none'}).inject(nav);

		// build the fancy links to NAV functions
		for (l=0;l<navList.length;l++) {
			// get the link text
			txt = navList[l];
			
			// build the element and inject
			// it into the container
			a = new Element('a',{
				// anchor attrs
				href: "javascript:g6nav." + this.navLinks[txt]+ "();" ,
				text: txt
			}).inject(Element("li",{"style":'text-align:left;text-decoration:none'}).inject($('navlist')));	
		}
		
		// show time
		return this.show();
	},

	/*-------------------
	 * Back button link
	 */
	backup:  function(){
		history.back();
		return this;
	},
	
	/*--------------------------
	 * check the results of a 
	 * remote request.  if 200
	 * then send to uri, else
	 * display message.
	 */
	sendThenGo: function(rslt,actionPhrase) {
    	// parse results text
    	js = JSON.decode(rslt);
    	rs = js.results.split("::");
    	
    	// check result code
    	if (rs[0] == '200') {
    		// goto main blog page
    		window.location.href=this.page + "?action=" + actionPhrase;
    	}
    	else {
        	// render messages (or hide empties)
    		$('messages').set('text',js.msg).set('class', "errorMsg");	
        }	            	
	},
	
	/*----------------------
	 * Don't ask, just go!
	 */
	loadPage: function(page,qstr) {
		window.location.href=this.page + "?" + qstr;
		return;
	},
	
	/*----------------------------
	 * It's an Action oriented 
	 * world out there.  The 
	 * Action Phrase is really
	 * just the Action name and 
	 * any parameters using the
	 * standard query string 
	 * format.
	 */
	loadAction: function(actionPhrase) {
		this.loadPage(this.page,"action=" + actionPhrase);
		return;
	}
	
});