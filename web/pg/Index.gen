<!DOCTYPE html>
<?python
from genshi.input import HTML 
?>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:xi="http://www.w3.org/2001/XInclude"
      xmlns:py="http://genshi.edgewall.org/">
    
    <py:if test="not data.has_key('chunk') or data['chunk'].lower() in ('false','0','')">
      	<xi:include href="../site_format_std.gen"/>
    </py:if>
   
	<head/>
	
	<body>
		<div class="bodytext">
			<h2>Welcome to the Next Level</h2>
			<p>
			  The <strong>PspyGear Framework</strong> is the latest rendition of the dynamic PspyWare 'Action Engine' that has been ported
			  to the <strong>Google AppEngine</strong> and allows <strong>Python</strong> programmers with all of tools needed to build robust, extensible
			  and extremely scalable web applications in the minimum amount of time. <strong>PspyGear</strong> provides a component based 
			  M-V-C style architecture with nicely defines layers for implementing reusable classes in well know patterns.
			  Built by developers for developers, we tried to put in all the power we wanted for ourselves with lots of room
			  for growth.  
			</p>
			
			<p>
			  <strong>PspyGear</strong> comes fully loaded with the <strong>MooTools</strong> JavaScripting libraries and the 
			  <strong>Genshi</strong> templating system to
			  make the web layers snap with fast, dynamic and reliable content generation that allow your pages to spring 
			  to life by utilizing <strong>AJAX</strong> on the front end and the <strong>PspyWare Action Framework</strong> in the middle layers while 
			  leveraging the Google's data modeling tools on the back end. A typical round-trip includes a post to the 
			  Action layer which is responsible for marshaling data between the web transport layers and the application's
			  Business Agents. The Agents provide the application's core functionality by managing the data objects and 
			  implementing the logic for the business rules and relationships. Once the functions have completed, the Agents
			  return results back to the Actions for packaging up either for direct JSON response or by using the <strong>Genshi</strong>
			  templates for generating web content. This pattern allows for maximum very rapid prototyping, development and 
			  debugging cycles and fits very nicely into most iterative SDLCs including scrum based <strong>Agile</strong> methodologies.  
			</p>
			
			<p>
			  The best part about <strong>PspyGear</strong> is the integration with the <strong>Google AppEngine</strong> for 
			  <strong>Python</strong> which provides all the
			  tools for a robust development environment and an extremely developer friendly production environment that is 
			  geared to allow the developer to build, test and promote the applications with little to no up front server charges. 
			  Even after your apps start getting enough traffic to attain critical mass, you won't be bleeding from the cost 
			  of maintaining the production environment. And if it goes viral, well scalability is provided by Google, 'nuff 
			  said!
			</p>
			
			<p>
			  And of course the entire <strong>PspyGear</strong> toolkit is <strong>Open Source Software</strong> sporting the 
			  <strong>Apache 2.0 License</strong> and housed in the <strong>Google 
			  Code</strong> world for easy access. Read through the Documentation pages (Getting Started, The Pro Guide and the Best Practices
			  Practices manuals are highly recommended), download the framework and the <strong>GAE SDK</strong> (Directions for that are in the Getting 
			  Started doc) and start building in the cloud today!
			</p>
		</div>
	</body>
</html>		
