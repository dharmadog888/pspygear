'''
Created on Aug 24, 2011

@author: david
'''
from pspy.gae import Server
from app.Ranger.domain import AppletRH as RangerRH
from app.pg.domain import AppletRH as PspyGearRH
from app.G6.domain import AppletRH as G6RH
from app.resume.domain import ResumeRH

# server start up params
APPLETS = [
('/Ranger/.*',RangerRH),('/Ranger',RangerRH),
('/PspyGear/.*',PspyGearRH),('/PspyGear',PspyGearRH),('/PG',PspyGearRH),
('/G6/.*',G6RH),('/G6',G6RH),
('/Resume/.*', ResumeRH),('/Resume', ResumeRH),
('/.*',PspyGearRH)
]

DEBUG_FLAG = True

THE_APP = Server(APPLETS,DEBUG_FLAG).getApp()
    