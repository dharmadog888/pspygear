'''
Created on Aug 25, 2011

@author: david
'''

from pspy.gae.actiondomain import PspyAction

class Index(PspyAction):
    """
    Default Action 
    spawns index.gen
    """
    def execute(self):
        req = self.request
        page = req.getParameter("page","Index")
        
        req.setAttribute("results","100::Ok")
        req.setResponsePage("/pg/%s.gen" % page)        
        
        return req.getResponse()
    
    def sess_check(self):
        return True