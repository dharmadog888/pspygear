'''
Created on Aug 25, 2011

@author: david
'''
from app.G6.actions import G6Action

class Blog(G6Action):
    """
    Default Action 
    spawns index.gen
    """
    def execute(self):
        req = self.request
        results = "200::OK"
        nxtPage = "/G6/blogger.gen"
        
        # validate session
        if not self.auth_check():
            results = "401::Invalid Session"
            nxtPage = "/G6/Index.gen"
            
        req.setAttribute("results",results)
        req.setResponsePage(nxtPage)        
        
        return req.getResponse()
