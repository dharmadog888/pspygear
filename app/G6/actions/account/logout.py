'''
Created on Sep 6, 2011

@author: david
'''
from app.G6.actions import G6Action

class Logout(G6Action):
    """
    Default Action 
    spawns index.gen
    """
    def execute(self):
        import logging
        
        # fetch session account
        req = self.request
        try:
            logging.debug("~~ Session:: %s " % str(req.session))
            acct = req.session.getAttribute('acct')
            
            # log it
            logging.info("++ Logging out %s" % acct.uid)
    
            # invalidate it
            del(req.session['acct'])
        except:
            logging.exception("!! Invalid Session")
            
        # return to front page
        req.setResponsePage("/G6/Index.gen")
        req.setAttribute("results","200::OK")
        
        # done
        return req.getResponse()
        
        
        