'''
Created on Sep 4, 2011

@author: david
'''
from pspy.gae.actiondomain import PspyAction
class G6Action(PspyAction):
    """
    Base Action class to handle
    session / auth checks for G6.
    """
    def auth_check(self):
        
        return self.request.session.has_key('g6acct')
    
    