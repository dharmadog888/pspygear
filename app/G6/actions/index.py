'''
Created on Aug 25, 2011

@author: david
'''
from app.G6.actions import G6Action

class Index(G6Action):
    """
    Default Action 
    spawns index.gen
    """
    def execute(self):
        req = self.request
    
        req.setAttribute("results","100::Ok")
        req.setResponsePage("/G6/Index.gen")        
        
        return req.getResponse()
    