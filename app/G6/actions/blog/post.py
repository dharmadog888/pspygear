'''
Created on Sep 6, 2011

@author: david
'''
from app.G6.actions import G6Action

class Post(G6Action):
    """
    Post a new article to blog
    """
    def execute(self):
        import logging
        from app.G6.agents import BlogAgent
        import json
        
        rslt = "200::Ok"
    
        if not self.auth_check():
            return self.JSONOutput(json.dumps({"results":"401::Invalid Session"}))

        try:    
            req = self.request
            sess = req.session
            agent = BlogAgent()
            
            blog = sess.getAttribute('currblog')
 
            subj = req.getParameter("subj")
            txt = req.getParameter("text")
            tags = req.getParameter("tags")
            start = req.getParameter("start",0)
            count = req.getParameter("count",agent.DEF_ARTICLES)
            
            agent.postArticle(blog, subj, txt, tags)
            
            logging.info("++ Article %s posted to blog %s" % (subj,blog.name))
            
            rv = {"results":rslt, "blog": blog.toJSON() };
                                                   
        except Exception,ex:
            logging.exception(str(ex))
            rv = json.dumps({"results":"500::Action Exception(%s)" % str(ex)}) 
                      
        # all done, send out the json
        return self.JSONOutput(json.dumps(rv))
        
