'''
Created on Sep 6, 2011

@author: david
'''
from app.G6.actions import G6Action

class Add(G6Action):
    """
    Create a new blog
    """
    def execute(self):
        import logging
        from app.G6.agents import BlogAgent
        
        req = self.request
        results = "200::OK"
        nxtPage = "/G6/blogger.gen"
        name = desc = tags = None
    
        # validate session
        if not self.auth_check():
            results = "401::Invalid Session"
            nxtPage = "/G6/Index.gen"
        else:
            acct = req.session.getAttribute('acct')
            # unpack
            name = req.getParameter("name")
            desc = req.getParameter('desc')
            tags = req.getParameter('tags')
            default = req.getParameter('default','0')
            
            logging.info("++ New Blog Request")
            # ask agent to create blog
            agent = BlogAgent()
            blog = agent.createBlog(acct, name, desc, tags, default)
            
            req.session.setAttribute("currblog",blog)
            
        # pack and send    
        req.setAttribute("results",results)
        req.setResponsePage(nxtPage)        
        
        return req.getResponse()
