'''
Created on Aug 25, 2011

@author: david
'''

from pspy.gae.actiondomain import PspyAction

class Index(PspyAction):
    """
    Default Action 
    spawns index.gen
    """
    def execute(self):
        req = self.request
        req.setAttribute("name",req.getParameter("name","Clouds"))
        
        req.setAttribute("results","200::Ok")
        req.setResponsePage("/hello/Index.gen")        
        
        return req.getResponse()
    
    def sess_check(self):
        return True