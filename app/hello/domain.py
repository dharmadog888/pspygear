'''
Created on Aug 24, 2011

@author: david
'''
import webapp2 as webapp
from webapp2_extras import sessions
from pspy.gae.actiondomain import ActionDomain
import logging

# Insults config
CFG = {"name":"Hello",'actionRoot':'./app/hello/actions', 'actionCache':'None', 'defAction':"Index", "webRoot":"./web", "htmlRoot":"./html" }

#class Domain(object):
class AppletRH(webapp.RequestHandler):
    @classmethod
    def getDomain(cls):
        return ActionDomain(CFG)
    
    def dispatch(self):
        """
        Override
        """
        self.session_store = sessions.get_store(request=self.request)
                
        try:
            webapp.RequestHandler.dispatch(self)
        finally:
            self.session_store.save_sessions(self.response)
            
    @webapp.cached_property
    def session(self):
        return self.session_store.get_session()

    def post(self):
        """
        Web Post Request
        """
        logging.debug("~~ Posted to %s" % CFG['name'])
        return self.process()
    
      
    def process(self):
        """
        Process web requests
        """
        ad = self.getDomain()
        rs = ad.processRequest(self.request, self.response, self.session)
        
        return rs
    
    def get(self):
        """
        Web Get Request
        """
        logging.debug("~~ Getting %s" % self.request.path)
        return self.process()
  
    