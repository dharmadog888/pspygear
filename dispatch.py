'''
Created on Aug 24, 2011

@author: david
'''
from pspy.gae import Server
from app.osgata.domain import AppletRH as OSGATARH
from app.G6.domain import AppletRH as G6RH
from app.bod.domain import AppletRH as BridgeOfDeathRH
from app.hello.domain import AppletRH as HelloRH
from app.default.domain import AppletRH as DefaultRH
# server start up params

APPLETS = [
('/ETT/.*',OSGATARH),('/ETT',OSGATARH),
('/G6/.*',G6RH),('/G6',G6RH),
('/BridgeOfDeath/.*',BridgeOfDeathRH),('/BridgeOfDeath',BridgeOfDeathRH),
('/Hello/.*', HelloRH),('/Hello', HelloRH),
("/.*",DefaultRH)
]

DEBUG_FLAG = True

# - PEP -    
if __name__ == "__main__":
    # start the server
    srvr = Server(APPLETS,DEBUG_FLAG)
    srvr.run()